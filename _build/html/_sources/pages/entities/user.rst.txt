..	_user:

Users
*****

Each connected client corresponds to a :class:`User` instance.


----

..  contents:: Users API Components

----


The Users Collection
====================

..  autoclass:: Users
    :members:

----

The User Instance
=================

..  autoclass:: User
    :members:

    .. autofunction:: Entity#hasPerm
    .. autofunction:: Entity.create
    .. autofunction:: Entity#update
    .. autofunction:: Entity#delete
